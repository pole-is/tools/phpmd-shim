# irstea/phpmd-shim - shim repository for phpmd/phpmd.

This package is a drop-in replacement for [phpmd/phpmd](https://phpmd.org/), which provides its PHAR archive as a binary.

It is built automatically from the official PHAR.

## Installation

	composer require irstea/phpmd-shim

or:

	composer require --dev irstea/phpmd-shim



## Usage

As you would use the original package, i.e. something like:

	vendor/bin/phpmd [options] [arguments]

## License

This distribution retains the license of the original software: BSD-3-Clause